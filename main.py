from MakeModule import MakeModule
from Tagging import Tagging
import sys

def mainMenu():
    flag = True
    while (flag):
        print('{0:*^50}'.format(' MENU '))
        print('* {0:<47}*'.format('1 -  Gerar Modulo '))
        print('* {0:<47}*'.format('2 -  Tagging'))
        print('* {0:<47}*'.format('0 -  Sair '))
        print('*' * 50)
        try:
            op = int(input(''))
        except Exception:
            op = ''

        if (op == 1):
            MenuGeraModulo()
        if (op == 2):
            Tagging()
        elif (op == '' or op == None):
            print('Informe uma opção')
        elif (op == 0):
            flag = False
            break

def MenuGeraModulo():
    flag = True
    while (flag):
        print('{0:*^50}'.format(' Ferramenta de gerenciamento de Backups PZM '))
        print('* {0:<47}*'.format('1 - Gerar Módulo de Conciliacao Contabil '))
        print('* {0:<47}*'.format('2 - Gerar Módulo de Fechamento Contabil '))
        print('* {0:<47}*'.format('3 - Gerar Módulo de Controle de Impostos '))
        print('* {0:<47}*'.format('4 - Gerar Módulo de Obrigações Acessórias '))
        print('* {0:<47}*'.format('5 - Gerar todos os Módulos '))
        print('* {0:<47}*'.format('0 -  Sair '))
        print('*' * 50)
        try:
            op = int(input(''))
        except Exception:
            op = ''

        if (op == 1):
            try:
                modulo = MakeModule('conciliacao')
                #modulo.makeBkp()
            except Exception:
                print("Erro inesperado: {}".format(sys.exc_info()))

        elif (op == 2):
            modulo = MakeModule('fechamento')
            modulo.makeBkp()
        elif (op == 3):
            modulo = MakeModule('controleImposto')
            modulo.makeBkp()
        elif (op == 4):
            modulo = MakeModule('obrigacoesAcessorias')
            modulo.makeBkp()
        elif (op == '' or op == None):
            print('Informe uma opção')
        elif (op == 0):
            flag = False
            mainMenu()

if __name__ == '__main__':
    mainMenu()